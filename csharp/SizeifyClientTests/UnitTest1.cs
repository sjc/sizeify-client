﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SJC;
using System.Threading.Tasks;

namespace SizeifyClientTests {
   [TestClass]
   public class UnitTest1 {
      string endpoint = "http://sizeifyb.sjc.io/";
      string imageURL = "http://placekitten.com/1000/";
      string resizeCode = "w50";

      [TestMethod]
      public void URLIsConstructedProperly() {
         // We are testing a private method so we need to use the Test Libs PrivateObject to gain access
         SJC.SizeifyClient sc = new SizeifyClient();
         PrivateObject obj = new PrivateObject(sc);
         object[] args = new object[3] { endpoint, imageURL, resizeCode };
         var retVal = obj.Invoke("formatURL", args);
         Assert.AreEqual("http://sizeifyb.sjc.io/http/com.placekitten/w50/1000", retVal);
      }

      [TestMethod]
      public async Task DataAsExpected() {
         // We are depending upon a 3rd party service. Our known image should be correctly encoded by the service and returned.
         SJC.SizeifyClient sc = new SizeifyClient();
         string retVal = "";
         retVal = await sc.Sizeify(
              endpoint,
              imageURL,
              resizeCode
           );
         Assert.AreEqual("data:image/jpeg; base64,/9j/4AAQSkZJRgABAQEAYABgAAD//gA7Q1JFQVRPUjogZ2QtanBlZyB2MS4wICh1c2luZyBJSkcgSlBFRyB2ODApLCBxdWFsaXR5ID0gOTAK/9sAQwADAgIDAgIDAwMDBAMDBAUIBQUEBAUKBwcGCAwKDAwLCgsLDQ4SEA0OEQ4LCxAWEBETFBUVFQwPFxgWFBgSFBUU/9sAQwEDBAQFBAUJBQUJFA0LDRQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQU/8AAEQgAMgAyAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A8o+If7B+t6Zj/hENGhvBI2yPyr4hoh/ek81gNvf5ST7Vh61/wTr+JEOniaz1TQr+4EQdrXz5I3390Usm0/UkV+lqQryMnPcUrwxqDngAZ5HFdjpxZz8z7n53+DP+CdevX/h5V8S3Nhp9+7bisLtLIg6Y3DC8D6iuW8X/APBNvx1pczHw/rGmavblsBLkvBIB+TA/mPpX6M3PjzwjbXQs5fE+iw3pOBA+oRK59tpbPY1vQLFcwxzROk0cgyskThlYeoPel7ONg52fkrd/sl/FX4TeKLS7k8OT65ZBcPcaKrXK4IwRtA3jBI/h7Gva/AP7M/jD4gz28uqWEnhvSCf3st5HtuCvokR5yfVsD69K/QA24GRz7YqEwHBG3A9Sa5qmDp1JqUj0KGYVsPTdOn1PHbL9lz4c2lnBA3hm1uGijVDNMMu5AxuY9yepor14wnP3jRXT7Kn/ACo4/b1f5n954x4l/bZ+FPh/TPtVnrU/iC5ZcxWmn2cu9vTJkVVX8Tn2r4L+Mv7SXjn4vaneHUdUurHRGkbydHifZCidg4XG8jjls89K/RCw/Z5+Fuk3pltPDlmJYjkCTc4z9Cea/Pj46+G5T411/XIfCur6L4fm1N4bW6uLR0il5I4JGMHDMPasqjmtwgonjgubgupZyUOQCK+iP2b/ANovxT8IdI1u3hYanYNGkltp95uaJZPmyVIOVzgZwcexrnfC37NHirxz4O1XxDpNukllYuqooyJLoZ+fylIydq9ffgZIxX17b/AD4c2ekabb3dqbS9FumZFmMbuQoyQf8Kx95K60NLxbszT/AGdP2ptW+N/iaDR77w5aaJOEllmCTvISir1AIG07ivXPGfSvo54imeenbNeYfDr4a6D4W8RXnii3t3m1vUUKvM7ZVEbBIjH8IO1e/Ye+fTo7kyDEhBPYsOcV2U+a3vHPK1/dF8t/7popxvBk8iitCCitvEkW9413Dk4HArlvH/wpsPiXos2larGJLO4X5lBwyHsy+hFd2ieY/D+/ApZeMkEqe/epsuoHkXgn4FSfDzw/b6TZa3e3YgBEby4QAHoPl9PfNcP8Uvg9rfjXQdUsL9755rc7raWJwqnPIKMvzZBUg98Pj0r6RHnFdqEGrMUZdU4C/hUOCasUpNO5xXwr8Ian4c8BaBp+ulX1G0tEgkKNu4H3VJ7kLgZ9RXW/2eByvHt6VblU7NucrnOc9KYDlgxOGIP5VaVlYTd3cpeU/wBf+BCirmQOzUVQiKH7zVBqBxbSfUfzFFFICewH7ofSpZSRnHHPaiimBBYuzXEmWJwT1NF6SNpzzmiigCLcfU0UUUAf/9k=", retVal);
      }

      [TestMethod]
      public async Task StringIsBase64() {
         SJC.SizeifyClient sc = new SizeifyClient();
         string retVal = "";
         retVal = await sc.Sizeify(
              endpoint,
              imageURL,
              resizeCode
           );
         Assert.IsTrue(retVal.IndexOf("data:image/jpeg; base64,") > -1, "msg");
      }

   }
}
